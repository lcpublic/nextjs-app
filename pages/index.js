import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import Footer from '../components/Footer'
import NavBar from '../components/NavBar'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <>
      <Head>
        <title> TDA | Spoons</title>
        <meta name="Keywords" content='TDA Spoons' />
      </Head>
      <div>
        <h1 className={styles.title}>Homepage</h1>
        <p className={styles.text}>This is a webpage built by me!</p>
        <p className={styles.text}>Whoop de la Whoop</p>
        <Link href="/spoons">
          <a className={styles.btn}> See All Spoons</a>
        </Link>
      </div>
    </>
  )
}
