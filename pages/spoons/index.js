import styles from '../../styles/Spoons.module.css'
import Link from "next/link";

export const getStaticProps = async () => {

    // const res = await fetch('https://spoon-blog.herokuapp.com/news')
    const res = await fetch('https://jsonplaceholder.typicode.com/users')
    const data = await res.json();

    return {
        props: { spoons: data }
    }

}

const Spoons = ({ spoons }) => {
    return (
        <div>
            <h1>All Spoons</h1>
            {spoons.map(eachSpoon => (
                <Link href={'/spoons/' + eachSpoon.id} key={eachSpoon.id}>
                    <h3 className={styles.single}>
                        {eachSpoon.name}
                    </h3>

                </Link>
            ))}
        </div>
    );
}

export default Spoons;
<div>
    <h1>All Spoons</h1>
</div>